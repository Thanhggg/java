import java.util.*;

public class build_palindrome_from_array {

public static void main(String []args){
 System.out.println("Hello World");

 System.out.println(solution("132244232"));
 System.out.println(solution("8199"));
 System.out.println(solution("123456789"));
 System.out.println(solution("8778966474"));
 System.out.println(solution("23213453463462352342352323423"));
 System.out.println(solution("232134000023"));
}

public static String solution(String input){
 StringBuilder output = new StringBuilder("");
 StringBuilder[] numberGroup = {new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder(),new StringBuilder()};
 for (int i = 0; i< input.length();i++){
     char item = input.charAt(i);
     int charGroup = Integer.parseInt(String.valueOf(item));
     numberGroup[charGroup].append(item);
 }
 
 for (int i = numberGroup.length - 1; i >=0; i--) {
     if (numberGroup[i].toString().length() <= 0) {
        continue;
     }
     if (output.toString().equals("")) {
         output.append(numberGroup[i].toString());
     }else {
         String currentGroupValue = numberGroup[i].toString();
         
         int outputMiddle = output.toString().length() / 2;
         boolean isOutputLengthIsOdd = output.toString().length() % 2 == 0;
         boolean isCurrentValueLengthIsOdd = currentGroupValue.length() % 2 == 0;
         
         if (isOutputLengthIsOdd) {
             output.insert(outputMiddle,currentGroupValue);
         } else {
             if (currentGroupValue.length() <= 1) {
                continue;
             }
             
             char outputMiddleValue = output.toString().charAt(outputMiddle);
             output.delete(outputMiddle,outputMiddle+1);
             String currentValueAfterCheck = isCurrentValueLengthIsOdd ? currentGroupValue.toString() : numberGroup[i].substring(0,numberGroup[i].length()-1);
            output.insert(outputMiddle,currentValueAfterCheck);
            int middleAfterAdd = output.toString().length()/2;
            output.insert(middleAfterAdd,outputMiddleValue);
         }
     }
 }
 
 return output.toString();
}
}